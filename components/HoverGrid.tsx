import HoverBox from "./HoverBox"

const HoverGrid = ({ width = 20, height = 20, size = 20, gap = 1 }: { width?: number, height?: number, size?: number, gap?: number }) => {
    return <div className="flex flex-col" style={{ gap }}>
        {[...Array(height)].map((_, y) => {
            return <div key={y} className="flex" style={{ gap }}>
                {[...Array(width)].map((_, x) => <HoverBox key={x} size={size} />)}
            </div>

        })}
    </div>
}

export default HoverGrid