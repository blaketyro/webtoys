const HoverBox = ({ size }: { size: number }) => {
    return <div style={{ width: size, height: size }}
        className="bg-gray-900 hover:bg-red-500 duration-[2000ms] ease-out hover:transition-colors"
    ></div>
}

export default HoverBox;