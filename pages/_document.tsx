import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en" className='h-screen'>
      <Head />
      <body className='h-screen bg-gray-700 text-gray-50'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
